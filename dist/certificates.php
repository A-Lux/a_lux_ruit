<?php require_once("header.php"); ?>
<main class="main">
    <div class="container">
        <div class="row">
        
        </div>
    </div>
    <div class="container ">
        <div class="row">
                <div class="cascade-slider_container" id="cascade-slider">
                        <div class="cascade-slider_slides">
                          <div class="cascade-slider_item">
                           
                            <img src="images/certificate.png" alt="">
                          </div>
                          <div class="cascade-slider_item">
                            
                            <img src="images/certificate4.png" alt="">
                          </div>
                          <div class="cascade-slider_item">
                           
                            <img src="images/certificate.png" alt="">
                          </div>
                          <div class="cascade-slider_item">
                            
                            <img src="images/certificate4.png" alt="">
                          </div>
                          <div class="cascade-slider_item">
                           
                            <img src="images/certificate.png" alt="">
                          </div>
                          <div class="cascade-slider_item">
                           
                            <img src="images/certificate4.png" alt="">
                          </div>
                        </div>
                  
            
                  
                        <span class="cascade-slider_arrow cascade-slider_arrow-left" data-action="prev"></span>
                        <span class="cascade-slider_arrow cascade-slider_arrow-right" data-action="next"></span>
                      </div>
        </div>
    </div>

    <script src="/js/glide.min.js"></script>
    <script src="/js/bundle.js"></script>
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
        integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
        crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
        integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1"
        crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
        integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM"
        crossorigin="anonymous"></script>
        <script src="/js/hc-offcanvas-nav.js"></script>
        <script src="/js/cascade-slider.js"></script>
    <script src="/js/main.js"></script>
    <script>
  $('#cascade-slider').cascadeSlider({
      
    });
    </script>
</main>

</html>