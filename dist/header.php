<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
        integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" href="css/main.css">
    <link rel="stylesheet" href="css/slick.css">
    <link rel="stylesheet" href="css/slick-theme.css">
    <link rel="stylesheet" href="css/hc-offcanvas-nav.css">
    <title>Document</title>
</head>
<nav id="main-nav" defer>
    <ul>
        <li><a href="./index.php">Главная</a></li>
            <li><a href="./catalog.php">Каталог</a></li>
            <li><a href="./about.php">О компании</a></li>
            <li><a href="./contacts.php">Контакты</a></li>
            <li><a href="./gallery.php">Галерея</a></li>
            <li><a href="./certificates.php">Сертификаты</a></li>
    </ul>
</nav>

<header class="header">
    <div class="container">
        <div class="row">
            <div class="col-sm-2">
                <div class="header__logo">
                    <a href="">
                        <img src="images/logo.svg" alt="">
                    </a>
                </div>
            </div>
            <div class="col-sm-7">
                <input type="text" class="search_field">
            </div>
            <div class="col-sm-3 px-0">
            <h2>
           
            </h2>
            </div>
        </div>
    </div>
</header>