const path = require("path");
const webpack = require('webpack');
const HtmlWebpackPlugin = require("html-webpack-plugin");

module.exports = {
   entry:"./src/js/main.js",
   output: {
       library: "helper",
       libraryTarget: "var",
       path: path.resolve(__dirname, "dist"),
       filename: "js/bundle.js"
   },
   plugins:[
    new webpack.ProvidePlugin({
      $: 'jquery',
      jQuery: 'jquery'
    })
],
   
   module:{
       rules:[
           {
               test: /\.js$/, 
               exclude: /node_modules/,       

           },
           
 
       ]

       

       
   }

};